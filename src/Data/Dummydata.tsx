import { CARDCOLOR1, CARDCOLOR2, CARDCOLOR3, CARDCOLOR4, CARDCOLOR5, CARDCOLOR6 } from "../Constants/Colors";

export const propertyData = [
    {
      title: "Property Data",
      color:CARDCOLOR1,
      image:require('../assets/propertyData.png'),
      children: [
       
        { title: "Inspection Report" },
      
      ]
    },

    {
        title: "Subject property",
        color:CARDCOLOR2,
        image:require('../assets/Group.png'),
      
            children: [
              { title: "Property"},
              { title: "Address" },
              { title: "Identification" }
            ]
      },
    {
      title: "Site Info",
      color:CARDCOLOR3,
      image:require('../assets/Group.png'),
    
          children: [
            { title: "Site Features", children: [{ title: "Buildings" }] },
            { title: "Offsite Features" },
            { title: "Site Utilities" }
          ]
    },
    


      {
        title: "Buildings",
        color:CARDCOLOR4,
        image:require('../assets/building.png'),
      
            children: [
              { title: "Building details" },
              { title: "Exterior Deficiencies" },
              { title: "Exterior updates" }
            ]
      },

      {
        title: "Units",
        color:CARDCOLOR5,
        image:require('../assets/Group.png'),
      
            children: [
              { title: "Unit Details" },
              { title: "Unit Features" },
              { title: "Site Utilities" }
            ]
      },


      {
        title: "Levels",
        color:CARDCOLOR6,
        image:require('../assets/levels.png'),
      
            children: [
              { title: "Level 1" }
            ]
      },
  ];


  export const SiteUtilityData = [
    {
      id:1,
      Title:'Electrical Services (1)',
    },
    {
      id:2,
      Title:'Sewer Services (1)'
    },
    {
      id:3,
      Title:'Water Services (1)'
    },
    {
      id:1,
      Title:'Gas Services (1)'
    }
  ]

 export const SiteFeatures = [
  {
    id:1,
    Title:'Locations'
  },
  {
    id:2,
    Title:'Site Views'
  },
  {
    id:3,
    Title:'Adverse Site Condtions'
  },

  ]