import React from 'react';
import { LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TabScreen from '../Screens/TabScreen';
import HomeScreen from '../Screens/HomeScreen';


// Ignore all log notifications
LogBox.ignoreAllLogs(true);

const Stack = createNativeStackNavigator();
const screenOptions = {
  headerShown: false,
};
const MainStack: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen" screenOptions={screenOptions}>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="TabScreen" component={TabScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MainStack;
