import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, Image } from 'react-native';
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles';
import {
  BACKGROUNDCOLOR,
  BACKGROUNDCOLOR2,
  PLACEHOLDERCOLOR,
  PRIMARYCOLOR,
  TEXTCOLOR1,
  VALIDCOLOR
} from '../../Constants/Colors'; //IMPORT COLORS
import { SiteFeatures, SiteUtilityData } from '../../Data/Dummydata'; //IMPORT DUMMY DATA

const SiteForm: React.FC = () => {

  return (
    <ScrollView style={styles.container}>
      <View style={styles.section}>
        <View style={styles.card}>
          <Text style={styles.sectionHeader}>Site Features</Text>
        </View>


        <FlatList
          data={SiteFeatures}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={styles.input}>
              <Text style={styles.textGps}>{item?.Title}</Text>
            </View>
          )} />


        <View style={styles.card}>
          <Text style={styles.sectionHeader}>Site Utilities</Text>
        </View>

        <FlatList
          data={SiteUtilityData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={styles.input}>
              <Text style={styles.textGps}>{item?.Title}</Text>
            </View>
          )} />

      </View>

    </ScrollView>
  );
};

export default SiteForm;

const styles = StyleSheet.create({
  container: {
    padding: DEFAULTWIDTH*0.04,
    backgroundColor: BACKGROUNDCOLOR,
  },
  section: {
    marginBottom: DEFAULTWIDTH*0.05,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  sectionHeader: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: DEFAULTWIDTH*0.04,
    color: BACKGROUNDCOLOR
  },
  card: {
    width: DEFAULTWIDTH * 0.90,
    backgroundColor: PRIMARYCOLOR,
    paddingLeft: 10,
    paddingTop: 8,
    marginBottom: DEFAULTWIDTH*0.05
  },
  input: {
    height: DEFAULTHEIGHT * 0.055,
    borderColor: PLACEHOLDERCOLOR,
    borderWidth: 1,
    marginBottom:DEFAULTWIDTH*0.04,
    paddingHorizontal:DEFAULTWIDTH*0.04,
    width: DEFAULTWIDTH * 0.90,
    color: TEXTCOLOR1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  errorText: {
    color: VALIDCOLOR,
    marginBottom: 10,

  },
  viewGps: {
    borderWidth: 1,
    borderColor: PLACEHOLDERCOLOR,
    width: DEFAULTWIDTH * 0.90,
    height: DEFAULTHEIGHT * 0.055,
    paddingHorizontal: 15,
    paddingTop: 8
  },
  textGps: {
    color: TEXTCOLOR1,
    fontSize: 14
  },
  subContainer: {
    borderWidth: 1,
    borderColor: PLACEHOLDERCOLOR,
    borderRadius: 4,
    margin: 10,
    width: DEFAULTWIDTH * 0.90
  },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: PLACEHOLDERCOLOR,
    padding: 10,
  },
  headerText: {
    fontSize: 14,
    color: TEXTCOLOR1
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: PLACEHOLDERCOLOR,
    backgroundColor: BACKGROUNDCOLOR2
  },
  cell: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  label: {
    fontSize: 14,
    color: TEXTCOLOR1
  },
  value: {
    fontSize: 14,
    color: TEXTCOLOR1
  },
  img:{
    width:DEFAULTWIDTH*0.05,
    height:DEFAULTHEIGHT*0.02  }
});
