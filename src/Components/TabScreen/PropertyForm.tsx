import React, { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { StyleSheet, Text, TextInput, View, Button, ScrollView, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles';
import { BACKGROUNDCOLOR, BACKGROUNDCOLOR2, PLACEHOLDERCOLOR, PRIMARYCOLOR, TEXTCOLOR1, VALIDCOLOR } from '../../Constants/Colors';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';

type FormValues = { //INITIALIZE THE VALUES
    collectionType: string;
    casefileId: string;
    LPAId: string;
    PDAEntity: string;
    collectorName: string;
    hyperLink: string;
    collectionEntity: string;
    collectorType: string;
    typeDesc: string;
    acknowledgement: string;
    collectionDate: string;
};

const PropertyForm: React.FC = () => {
    const { control, handleSubmit, formState: { errors } } = useForm<FormValues>();

    const onSubmit = async (data: FormValues) => { //STORE DATA INTO ASYNC STORAGE
        try {
            await AsyncStorage.setItem('@propertyData', JSON.stringify(data));
            console.log('Data saved successfully:', data);
        } catch (e) {
            console.error('Failed to save data:', e);
        }
    };

    useEffect(() => {
        const checkConnectivityAndUpload = async () => { //CHECK NETWORK CONNECTIVITY 
            const storedData = await AsyncStorage.getItem('@propertyData');
            if (storedData) {
                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        uploadData(JSON.parse(storedData));
                    }
                });
            }
        };

        const uploadData = async (data: FormValues) => { //UPLOAD INTO APO (DUMMY CASE)
            try {
                const response = await axios.post('YOUR_API_ENDPOINT', data);
                if (response.status === 200) {
                    await AsyncStorage.removeItem('@propertyData');
                    Alert.alert('Success', 'Data uploaded successfully');
                }
            } catch (error) {
                console.error('Failed to upload data:', error);
                Alert.alert('Error', 'Failed to upload data')
                AsyncStorage.removeItem('@propertyData');
            }
        };

        const unsubscribe = NetInfo.addEventListener(state => {
            if (state.isConnected) {
                checkConnectivityAndUpload();
            }
        });

        return () => unsubscribe();
    }, []);


    return (
        <ScrollView style={styles.container}>
            <View style={styles.section}>

                <View style={styles.card}>
                    <Text style={styles.sectionHeader}>Inspection Report</Text>
                </View>

                <Controller
                    control={control}
                    name="collectionType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Collection Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.collectionType && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="casefileId"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Case File ID"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.casefileId && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="LPAId"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="LPA ID"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.LPAId && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="PDAEntity"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="PDF Submitter Entity"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.PDAEntity && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="hyperLink"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="PDF Hyperlink"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.hyperLink && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="collectionEntity"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="PDF Collection Entity"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.collectionEntity && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="collectionEntity"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="PDA Collection entity"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.collectionEntity && <Text style={styles.errorText}>This is required.</Text>}


                <Controller
                    control={control}
                    name="typeDesc"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Property Data Collector Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.typeDesc && <Text style={styles.errorText}>This is required.</Text>}


                <Controller
                    control={control}
                    name="acknowledgement"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Data Collector Acknowledgement"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.acknowledgement && <Text style={styles.errorText}>This is required.</Text>}


                <Controller
                    control={control}
                    name="collectionDate"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Data Collection Date"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.collectionDate && <Text style={styles.errorText}>This is required.</Text>}
            </View>

            <Button title="Submit" onPress={handleSubmit(onSubmit)} color={PRIMARYCOLOR} />
        </ScrollView>
    );
};

export default PropertyForm;

const styles = StyleSheet.create({
    container: {
        padding: 16,
        backgroundColor: BACKGROUNDCOLOR,
    },
    section: {
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    sectionHeader: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
        color: BACKGROUNDCOLOR
    },
    card: {
        width: DEFAULTWIDTH * 0.90,
        backgroundColor: PRIMARYCOLOR,
        paddingLeft: 10,
        paddingTop: 8,
        marginBottom: 10
    },
    input: {
        height: DEFAULTHEIGHT * 0.055,
        borderColor: PLACEHOLDERCOLOR,
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        width: DEFAULTWIDTH * 0.90,
        color: TEXTCOLOR1

    },
    errorText: {
        color: VALIDCOLOR,
        marginBottom: 10,

    },
    viewGps: {
        borderWidth: 1,
        borderColor: PLACEHOLDERCOLOR,
        width: DEFAULTWIDTH * 0.90,
        height: DEFAULTHEIGHT * 0.055,
        paddingHorizontal: 15,
        paddingTop: 8
    },
    textGps: {
        color: TEXTCOLOR1,
        fontSize: 14
    },
    subContainer: {
        borderWidth: 1,
        borderColor: PLACEHOLDERCOLOR,
        borderRadius: 4,
        margin: 10,
        width: DEFAULTWIDTH * 0.90
    },
    header: {
        borderBottomWidth: 1,
        borderBottomColor: PLACEHOLDERCOLOR,
        padding: 10,
    },
    headerText: {
        fontSize: 14,
        color: TEXTCOLOR1
    },
    row: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: PLACEHOLDERCOLOR,
        backgroundColor: BACKGROUNDCOLOR2
    },
    cell: {
        flex: 1,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    label: {
        fontSize: 14,
        color: TEXTCOLOR1
    },
    value: {
        fontSize: 14,
        color: TEXTCOLOR1
    },
});
