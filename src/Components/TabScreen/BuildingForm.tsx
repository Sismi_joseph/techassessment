import React, { useEffect } from 'react'
import {
    Alert,
    Button,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native'
import {
    BACKGROUNDCOLOR,
    BACKGROUNDCOLOR3,
    GREYCOLOR,
    PLACEHOLDERCOLOR,
    PRIMARYCOLOR,
    TEXTCOLOR1,
    VALIDCOLOR
} from '../../Constants/Colors'
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles'

import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';
import { Controller, useForm } from 'react-hook-form';
import AsyncStorage from '@react-native-async-storage/async-storage';

type FormValues = { //INITIALIZE THE VALUES
    structureType: string;
    structureArea: string;
    attachType: string;
    foundType: string;
    constructionStatus: string;
    constructionType: string;
    rooms: string;
    yearsBuilt: string;
    estimate: string;
    design: string;
    desc: string;
    projectName: string;
    buildNum: string,
    stories: string,
    elevators: string,
    offstreet: string,
    storageType: string,
    exteriorDef: string,
    updates: string
};

const BuildingForm: React.FC = () => {

    const { control, handleSubmit, formState: { errors } } = useForm<FormValues>();

    const onSubmit = async (data: FormValues) => { //SUBMIT THE DATA INTO ASYNC STORAGE
        try {
            await AsyncStorage.setItem('@BuildData', JSON.stringify(data));
            console.log('Data saved successfully:', data);
        } catch (e) {
            console.error('Failed to save data:', e);
        }
    };

    useEffect(() => {
        const checkConnectivityAndUpload = async () => { //CHECK NETWORK CONNECTIVITY 
            const storedData = await AsyncStorage.getItem('@BuildData');
            if (storedData) {
                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        uploadData(JSON.parse(storedData));
                    }
                });
            }
        };

        const uploadData = async (data: FormValues) => { //UPLOAD INTO APO (DUMMY CASE)
            try {
                const response = await axios.post('YOUR_API_ENDPOINT', data);
                if (response.status === 200) {
                    await AsyncStorage.removeItem('@BuildData');
                    Alert.alert('Success', 'Data uploaded successfully');
                }
            } catch (error) {
                console.error('Failed to upload data:', error);
                Alert.alert('Error', 'Failed to upload data')
                AsyncStorage.removeItem('@BuildData');
            }
        };

        const unsubscribe = NetInfo.addEventListener(state => {
            if (state.isConnected) {
                checkConnectivityAndUpload();
            }
        });

        return () => unsubscribe();
    }, []);

    return (
        <View>

            <View style={styles.subView}>

                <View style={styles.row}>
                    <TouchableOpacity style={[styles.card, { backgroundColor: PRIMARYCOLOR }]}>
                        <Text style={styles.textBuild}>#Building 1</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.card}>
                        <Text style={[styles.textBuild, { color: PLACEHOLDERCOLOR }]}>#Building 2</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.card}>
                        <Text style={[styles.textBuild, { color: PLACEHOLDERCOLOR }]}>+ Add</Text>
                    </TouchableOpacity>

                </View>

            </View>

            <View style={{ margin: 10 }}>
                <TouchableOpacity style={styles.detailCard}>
                    <Text style={styles.textBuild}>Building Details</Text>
                </TouchableOpacity>
            </View>





            <View style={styles.cardView}>

                <Controller
                    control={control}
                    name="structureType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Structure Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.structureType && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="structureArea"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Structure Area"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.structureArea && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="attachType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Attachment Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.attachType && <Text style={styles.errorText}>This is required.</Text>}


                <Controller
                    control={control}
                    name="constructionStatus"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Construction Status"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.constructionStatus && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="constructionType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Construction Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.constructionType && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="rooms"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Contains Roons"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.rooms && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="yearsBuilt"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Years Built"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.yearsBuilt && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="design"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Building Design"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.design && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="desc"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Building Design Description"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.desc && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="projectName"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Project Name"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.projectName && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="buildNum"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Building Number"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.buildNum && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="stories"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Number of Stories"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.stories && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="elevators"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Number of Elevators"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.elevators && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="offstreet"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Off street parking available"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.offstreet && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="storageType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Condo Car Storage Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.storageType && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="estimate"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Years Built Estimate"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.estimate && <Text style={styles.errorText}>This is required.</Text>}


                <Controller
                    control={control}
                    name="foundType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Foundation Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.foundType && <Text style={styles.errorText}>This is required.</Text>}

                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={styles.detailCard}>
                        <Text style={styles.textBuild}>Exterior Deficiencies</Text>
                    </TouchableOpacity>
                </View>

                <Controller
                    control={control}
                    name="exteriorDef"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Exterior Deficiencies"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.exteriorDef && <Text style={styles.errorText}>This is required.</Text>}



                <View style={{ margin: 10 }}>
                    <TouchableOpacity style={styles.detailCard}>
                        <Text style={styles.textBuild}>Exterior Updates</Text>
                    </TouchableOpacity>
                </View>

                <Controller
                    control={control}
                    name="updates"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Exterior Updates"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                {errors.updates && <Text style={styles.errorText}>This is required.</Text>}

                <Button title="Submit" onPress={handleSubmit(onSubmit)} color={PRIMARYCOLOR} />
            </View>


        </View>
    )
}

export default BuildingForm

const styles = StyleSheet.create({
    cardView: {
        margin: DEFAULTWIDTH*0.05
    },
    subView: {
        backgroundColor: GREYCOLOR,
        width: DEFAULTWIDTH,
        height: DEFAULTHEIGHT * 0.1,
    },
    card: {
        alignItems: 'center',
        justifyContent: 'center',
        width: DEFAULTWIDTH * 0.3,
        height: DEFAULTHEIGHT * 0.06,
    },
    textBuild: {
        color: BACKGROUNDCOLOR,
        fontSize: 16
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin:DEFAULTWIDTH*0.04
    },
    input: {
        height: DEFAULTHEIGHT * 0.055,
        borderColor: PLACEHOLDERCOLOR,
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        width: DEFAULTWIDTH * 0.50,
        marginLeft:DEFAULTWIDTH*0.05,
        color: TEXTCOLOR1
    },
    errorText: {
        color: VALIDCOLOR,
        marginBottom: DEFAULTWIDTH*0.05,
        paddingLeft:DEFAULTWIDTH*0.05
    },
    detailCard: {
        backgroundColor: BACKGROUNDCOLOR3,
        width: DEFAULTWIDTH * 0.4,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,

    }
})