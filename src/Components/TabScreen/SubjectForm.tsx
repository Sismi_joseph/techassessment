import React,{useEffect} from 'react';
import { Controller, useForm } from 'react-hook-form';
import { StyleSheet, Text, TextInput, View, Button, ScrollView,Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles';
import { BACKGROUNDCOLOR, BACKGROUNDCOLOR2, PLACEHOLDERCOLOR, PRIMARYCOLOR, TEXTCOLOR1, VALIDCOLOR } from '../../Constants/Colors';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';

type FormValues = { //INITIALIZE THE VALUES
    propertyType: string;
    propertyOccupied: string;
    streetAddress: string;
    city: string;
    county: string;
    state: string;
    postalCode: string;
    unitNumber: string;
   
};

const SubjectForm: React.FC = () => {
    const { control, handleSubmit, formState: { errors } } = useForm<FormValues>();

    const onSubmit = async (data: FormValues) => {
        try {
            await AsyncStorage.setItem('@form_data', JSON.stringify(data));
            console.log('Data saved successfully:', data);
        } catch (e) {
            console.error('Failed to save data:', e);
        }
    };

    useEffect(() => {
        const checkConnectivityAndUpload = async () => { //CHECK NETWORK CONNECTIVITY 
            const storedData = await AsyncStorage.getItem('@form_data');
            if (storedData) {
                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        uploadData(JSON.parse(storedData));
                    }
                });
            }
        };

        const uploadData = async (data: FormValues) => { //UPLOAD INTO APO (DUMMY CASE)
            try {
                const response = await axios.post('YOUR_API_ENDPOINT', data);
                if (response.status === 200) {
                    await AsyncStorage.removeItem('@form_data');
                    Alert.alert('Success', 'Data uploaded successfully');
                }
            } catch (error) {
                console.error('Failed to upload data:', error);
                Alert.alert('Error','Failed to upload data')
                AsyncStorage.removeItem('@form_data');
            }
        };

        const unsubscribe = NetInfo.addEventListener(state => {
            if (state.isConnected) {
                checkConnectivityAndUpload();
            }
        });

        return () => unsubscribe();
    }, []);

    return (
        <ScrollView style={styles.container}>
            <View style={styles.section}>

                <View style={styles.card}>
                    <Text style={styles.sectionHeader}>Property</Text>
                </View>

                <Controller
                    control={control}
                    name="propertyType"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Property Type"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.propertyType && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="propertyOccupied"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Property Occupied"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
            </View>

            <View style={styles.section}>
                <View style={styles.card}>
                    <Text style={styles.sectionHeader}>Address</Text>
                </View>
                <Controller
                    control={control}
                    name="streetAddress"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Street Address"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.streetAddress && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="city"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="City"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.city && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="county"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="County"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />

                <Controller
                    control={control}
                    name="state"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="State"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.state && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="postalCode"
                    rules={{ required: true }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Postal Code"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
                {errors.postalCode && <Text style={styles.errorText}>This is required.</Text>}

                <Controller
                    control={control}
                    name="unitNumber"
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Unit Number"
                            placeholderTextColor={PLACEHOLDERCOLOR}
                        />
                    )}
                />
            </View>

            <View style={styles.section}>
                <View style={styles.card}>
                    <Text style={styles.sectionHeader}>Identification</Text>
                </View>

                <View style={styles.subContainer}>
                    <View style={styles.header}>
                        <Text style={styles.headerText}>GPS Coordinates</Text>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.cell}>
                            <Text style={styles.label}>Latitude</Text>
                            <Text style={styles.value}>34° 17' 36.708" N</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.cell}>
                            <Text style={styles.label}>Longitude</Text>
                            <Text style={styles.value}>97° 32' 56.598" W</Text>
                        </View>
                    </View>
                </View>

            </View>

            <Button title="Submit" onPress={handleSubmit(onSubmit)} color={PRIMARYCOLOR} />
        </ScrollView>
    );
};

export default SubjectForm;

const styles = StyleSheet.create({
    container: {
        padding: 16,
        backgroundColor: BACKGROUNDCOLOR,
    },
    section: {
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    sectionHeader: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
        color: BACKGROUNDCOLOR
    },
    card: {
        width: DEFAULTWIDTH * 0.90,
        backgroundColor: PRIMARYCOLOR,
        paddingLeft: 10,
        paddingTop: 8,
        marginBottom: 10
    },
    input: {
        height: DEFAULTHEIGHT * 0.055,
        borderColor: PLACEHOLDERCOLOR,
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        width: DEFAULTWIDTH * 0.90,
        color:TEXTCOLOR1

    },
    errorText: {
        color: VALIDCOLOR,
        marginBottom: 10,
    },
    viewGps: {
        borderWidth: 1,
        borderColor: PLACEHOLDERCOLOR,
        width: DEFAULTWIDTH * 0.90,
        height: DEFAULTHEIGHT * 0.055,
        paddingHorizontal: 15,
        paddingTop: 8
    },
    textGps: {
        color: TEXTCOLOR1,
        fontSize: 14
    },
    subContainer: {
        borderWidth: 1,
        borderColor:PLACEHOLDERCOLOR,
        borderRadius: 4,
        margin: 10,
        width:DEFAULTWIDTH*0.90
      },
      header: {
        borderBottomWidth: 1,
        borderBottomColor:PLACEHOLDERCOLOR,
        padding: 10,
      },
      headerText: {
        fontSize:14,
        color:TEXTCOLOR1
      },
      row: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor:PLACEHOLDERCOLOR,
        backgroundColor:BACKGROUNDCOLOR2
      },
      cell: {
        flex: 1,
        padding: 10,
        flexDirection:'row',
        justifyContent:'space-between',
      },
      label: {
        fontSize:14,
        color:TEXTCOLOR1
      },
      value: {
        fontSize:14,
        color:TEXTCOLOR1
      },
});
