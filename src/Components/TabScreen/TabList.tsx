import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { BACKGROUNDCOLOR, PLACEHOLDERCOLOR, TEXTCOLOR1 } from '../../Constants/Colors'
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles'
import PropertyForm from './PropertyForm';
import SubjectForm from './SubjectForm';
import SiteInfoForm from './SiteInfoForm';
import BuildingForm from './BuildingForm';


const TabList: React.FC = () => {

    const TabHeading = [
        {
            id: 1,
            Title: 'Property Data',
            image: require('../../assets/propertyData.png')
        },
        {
            id: 2,
            Title: 'Subject property',
            image: require('../../assets/subject.png')
        },
        {
            id: 3,
            Title: 'Site Info',
            image: require('../../assets/Group.png')
        },
        {
            id: 4,
            Title: 'Buildings',
            image: require('../../assets/building.png')
        },
        {
            id: 5,
            Title: 'Units',
            image: require('../../assets/propertyData.png')
        },
    ]

    const [tabValue, setTabValue] = useState<number>(1);

    const handleSetTabValue = (id: number) => {
        setTabValue(id);
    };

    return (
        <View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {TabHeading?.map((item, index) => {
                    return (
                        <TouchableOpacity
                            key={index}
                            style={[styles.touchCard,
                            {
                                backgroundColor: item.id === tabValue ?
                                    PLACEHOLDERCOLOR : BACKGROUNDCOLOR
                            }]}
                            onPress={() => handleSetTabValue(item.id)}>
                            <Image source={item.image} />
                            <Text style={styles.textTitles}>{item?.Title}</Text>
                        </TouchableOpacity>
                    )
                })}
            </ScrollView>

            <View>
                {tabValue === 1 ?
                    <PropertyForm /> :
                    tabValue === 2 ?
                        <SubjectForm /> :
                        tabValue === 3 ?
                            <SiteInfoForm /> :
                            tabValue === 4 ?
                                <BuildingForm /> : null}
            </View>
        </View>
    )
}

export default TabList

const styles = StyleSheet.create({
    textTitles: {
        color: TEXTCOLOR1,
        fontSize: 12
    },
    touchCard: {
        width: DEFAULTWIDTH * 0.2,
        height: DEFAULTHEIGHT * 0.1,
        backgroundColor: BACKGROUNDCOLOR,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
