import { Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { PRIMARYCOLOR, TEXTCOLORHEADER } from '../../Constants/Colors'
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles'


const Header: React.FC = () => {
  return (
    <>
      <StatusBar backgroundColor={PRIMARYCOLOR} barStyle={'light-content'} />
      <View style={styles.mainView}>
        <View style={styles.subView}>

          <TouchableOpacity>
        <Image source={require('../../assets/arrow-left.png')}/>
          </TouchableOpacity>

          <Text style={styles.headerText}>Green Sky Apartment</Text>
        </View>

        <View>
          <TouchableOpacity>
          <Image source={require('../../assets/more-alt.png')}/>
          </TouchableOpacity>
        </View>
      </View>
    </>
  )
}

export default Header

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: PRIMARYCOLOR,
    width: DEFAULTWIDTH,
    height: DEFAULTHEIGHT * 0.08,
    paddingLeft: DEFAULTWIDTH * 0.05,
    paddingRight: DEFAULTWIDTH * 0.05,
    alignItems: 'center'
  },
  headerText: {
    color: TEXTCOLORHEADER,
    fontSize: 16,
    marginLeft: DEFAULTWIDTH * 0.04
  },
  subView: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})