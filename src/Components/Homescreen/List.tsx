import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { propertyData } from '../../Data/Dummydata';
import { DEFAULTHEIGHT, DEFAULTWIDTH } from '../../Constants/styles';
import { TEXTCOLOR1 } from '../../Constants/Colors';

type ListProps = {
    navigation: any;
  };
  
const List: React.FC<ListProps> = ({ navigation }) => {

    return (
        <View style={{padding:10}}>
            <FlatList
                data={propertyData}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
               
                renderItem={({ item }) => (

                    <TouchableOpacity onPress={() => navigation.navigate('TabScreen', { item })}
                    style={[styles.itemContainer, { backgroundColor: item?.color }]}>
                        <Image 
                        resizeMode='contain'
                        source={item?.image} 
                        style={styles.img}/>
                        <Text style={styles.itemText}>{item.title}</Text>

                        {item?.children?.map((i, index) => {
                            return (
                                <View key={index}>
                                    <Text style={styles.subTitle}>{i?.title}</Text>
                                </View>
                            )
                        })}
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

export default List

const styles = StyleSheet.create({
    itemContainer: {
        width: DEFAULTWIDTH * 0.415,
        padding:DEFAULTWIDTH*0.1,
        paddingLeft:DEFAULTWIDTH*0.07,
        alignItems:'flex-start',
        margin:10
    },
    itemText: {
        color: TEXTCOLOR1,
        fontWeight:'500',
        fontSize:14,
        marginBottom:DEFAULTHEIGHT*0.02
    },
    subTitle:{
        color:TEXTCOLOR1,
        fontWeight:'400',
        fontSize:12
    },
    img:{
        width:DEFAULTWIDTH*0.1,
        height:DEFAULTHEIGHT*0.05,
        marginBottom:10
    }
})