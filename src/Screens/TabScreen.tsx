import { StyleSheet, Text, View, SafeAreaView, ScrollView } from 'react-native';
import React from 'react';
import Header from '../Components/Common/Header';
import TabList from '../Components/TabScreen/TabList';

const TabScreen: React.FC = () => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header />
        <View style={styles.tabListContainer}>
          <TabList />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TabScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff', // or your desired background color
  },
  tabListContainer: {
    flex: 1,
    //padding: 16, // adjust as needed
  },
});
