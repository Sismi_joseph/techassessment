import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import React from 'react';
import Header from '../Components/Common/Header';
import { BACKGROUNDCOLOR } from '../Constants/Colors';
import List from '../Components/Homescreen/List';

const HomeScreen: React.FC<{ navigation: any }> = ({ navigation }) => {
    return (
      <SafeAreaView>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Header />
          <View style={styles.content}>
            <List navigation={navigation} />
          </View>
        </ScrollView>
      </SafeAreaView>
    )
};

export default HomeScreen;

const styles = StyleSheet.create({
    content: {
        backgroundColor: BACKGROUNDCOLOR,
        flex:1
    },
});
