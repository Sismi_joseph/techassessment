import React from 'react';
import MainStack from './src/Navigation/mainStack';

const App: React.FC = () => {
  return (
    <MainStack />
  );
}

export default App;
